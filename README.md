# Processor Architectures

| Processor | Price | Used In | Notes |
| --------- | ----- | ------- | ----- |
| XMega E8  |       |         |       |
| ATSAM21D  |       |         |       |
| ATSAM51D  |       |         |       | 